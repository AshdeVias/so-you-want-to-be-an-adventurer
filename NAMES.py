import random

dict_Fantasy_Race = {
    'dwarf':  { 
        'firstname': [
            'Gritila','Thingini','Nalbura','Dona','Dwnia',
            'Bultila','Thgini','Fimi','Herbura','Dalgari',
            'Bolona','Grudina','Garria','Simila','Ovon',
            'Chalon','Dwingoli','Ovola','Ketrimi','Drudria'
            ],
		'lastname' : [ 
            'Thingini','Norili','Bilona','Bulala','Calola',
            'Hagrima','Gilraka','Noria','Hardina','Norip','Ketkona',
            'Thintila','Rununni','Bila','Bolina','Ragi','Gribura','Fartria','Bolala','Mungrima'] 
    },
    'elf': { 
        'firstname': [ 
            'Aithlin', 'Aredhel', 'Chathanglas', 'Bialaer', 'Cluhurach', 'Dannyd', 'Daealla', 
            'Drannor', 'Eldaernth', 'Elkhazel', 'Erendriel', 'Galan', 'Halanaestra', 'Halueve', 
            'Ievos', 'Juppar', 'Katar', 'Larongar', 'Laosx', 'Luirlan'
            ], 
        'lastname': [ 
            'Luvon', 'Lyklorm', 'Mi’tilarro', 'Miilaethorn', 'Tordynnar', 
            'Dyffros', 'Daenestra', 'Círdan', 'Allannia', 'Alys', 'Ryfon', 'Ryvvik', 'Raerauntha', 
            'Talaedra', 'Talila', 'Veara', 'Vestele', 'Vhoorhin', 'Yalanilue', 'Zabbas'
            ]
    },
    'goblin': { 
        'firstname': [ 
            'Akbug', 'Argav', 'Brigadve', 'Corbakl', 'Cruncha', 'Frum', 
            'Gelmax', 'Glibl', 'Glogroth', 'Bloov', 'Glovd', 'Gorf', 'Grelth', 
            'Grickstah', 'Griga', 'Groovilla', 'Dar', 'Trog', 'Khroongah' 
            ],
        'lastname':  ['Kosrik', 'Makdur', 'Porgl', 'Throngul', 'Thuk', 'Tryxtah', 
        'Vorlag', 'Yorvua', 'Throgpull', 'Getanr', 'Rzonb', 'Zekaxg', 'Kom', 
        'Dkezor', 'Kgum', 'Kzaxn', 'Bemk', 'Dazx', 'Mtoxk', 'Rdab' 
        ]
    }
}

#space and tab
e = "\n\t>> "

def main(debug = False):
    if debug: 
        return "thisbetest"
    your_name = input("\nWhat is your name?" + e)

    print( "\nHello, " + your_name + ", Lets get you started with your adventure character.") 
    print("\nWhat fantasy race do you like?")
        
    call_to_adventure = True
    while call_to_adventure: 
        
        user_selection = input(str(list(dict_Fantasy_Race.keys())) + e)  
            
        if user_selection.lower() in dict_Fantasy_Race.keys():

            fn = random.choice((dict_Fantasy_Race[user_selection.lower()]['firstname']))
            ln = random.choice((dict_Fantasy_Race[user_selection.lower()]['lastname']))

            print("\nYou have chosen " + user_selection + "! " + "Your new name is " + fn + " " + ln + "\n")
            call_to_adventure = False
            return fn + " " + ln

        else:
            print("\nReading is hard, isn't it? What fantasy race do you like?!")
            print( "\nYou picked, " + user_selection + " Please pick from list below")
             
    input("Press any key to close program")









