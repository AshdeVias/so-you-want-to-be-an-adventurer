from collections import OrderedDict as OD
import NAMES
import random
import collections

adventurer = ({
    "Barbarian" :{ "strength" :"", "constitution": "","dexterity": "", "wisdom": "", "charismaisma" : "","intelligence" : ""
    },
    "Cleric" : {"wisdom" :"","constitution" :"","strength" :"", "intelligence" :"", "charisma" :"", "dexterity" :"" 
    },
    "Wizard" : {"intelligence" :"","dexterity" :"","constitution" :"", "wisdom" :"", "charisma" :"", "strength" :""
    },
})

ordered_dict = OD(adventurer)
us = NAMES.main(debug = False)
e = NAMES.e

def highroller():
    print("Ok, " + us + " lets pick your adventurer class!")

    lets_get_rollin = True
    while lets_get_rollin:
        classes = input(str(list(adventurer.keys())) + e)
        stat = OD(adventurer[classes.title()])
        please = ['roll']
        d20 = [ random.randint(0, 20) for i in range(6) ]
        d20.sort(reverse=True)
        for key in stat.keys():
                stat[key] = d20.pop(0)
  
        if classes.title() in adventurer.keys(): 
            print("\nI see, you're a " + classes + "! Lets roll for your STATS!\n")
            lets_roll = input((("Type roll to get your, well rolls...")) + e)

            d20_time = True
            while d20_time: 
                if lets_roll.lower() in please:
                    print('So, ' + us + ' you are a ' + classes + ' and these are your stats: \n')

                    for key, value in stat.items():
                        print("{}: {}".format(key, value))

                    print('\nLets go on an adventure')
                        
                    d20_time = False

                else:
                    print("\nTry again, I said type roll not " + lets_roll)  

                    input(" ")

            lets_get_rollin = False
        
        else:
            print("\nUM " + classes + " wasn't an option but ok. Pick from the list!!")

    input('\nPress any key to continue')
highroller()